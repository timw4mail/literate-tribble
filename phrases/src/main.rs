extern crate phrases;

use phrases::english::greetings as en_greetings;
use phrases::english::farewells as en_farewells;
use phrases::japanese::greetings as ja_greetings;
use phrases::japanese::farewells as ja_farewells;


fn main() {
	println!("Hello in English; {}", en_greetings::hello());
    println!("And in Japanese: {}", ja_greetings::hello());
	println!("Goodbye in English: {}", en_farewells::goodbye());
	println!("And in Japanese: {}", ja_farewells::goodbye());
}